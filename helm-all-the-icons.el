;;; helm-all-the-icons.el --- Helm integration with all-the-icons  -*- lexical-binding: t; -*-

;; Copyright (C) 2020 Ivan Yonchovski, Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Keywords: convenience

;; Version: 0.1
;; URL: https://gitlab.com/andersjohansson/helm-all-the-icons
;; Package-Requires: ((emacs "25.1") (dash "2.14.1") (f "0.20.0") (all-the-icons "0.1"))
;; Created 2020-05-22
;; Modified: 2020-09-02

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received candidate copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Integration of helm with all-the-icons
;; Add sources that you want to be iconified to
;; ‘helm-all-the-icons-files-sources’ for sources with filenames and
;; ‘helm-all-the-icons-buffer-sources’ for sources with buffer names.
;; Load helm, load, helm-all-the-icons and invoke ‘helm-all-the-icons-enable’
;; Based on: https://github.com/yyoncho/helm-icons

;;; Code:
(require 'seq)
(require 'subr-x)
(require 'cl-lib)

(require 'all-the-icons)
(require 'dash)
(require 's)

;; Can’t have that function memoized when doing it so much. It consumes a
;; lot of memory and is slow (at least adding initial values to the memoize-hashtable...).

;; (memoize-restore #'all-the-icons-icon-for-file)

(defgroup helm-all-the-icons nil
  "All-the-icons icons for helm"
  :group 'helm)

(defcustom helm-all-the-icons-files-sources
  '(helm-source-recentf
    helm-source-find-files
    helm-source-locate
    helm-source-files-in-current-dir)
  "Helm sources with filenames to which icons should be added."
  :type '(repeat symbol))

(defcustom helm-all-the-icons-buffer-sources '(helm-source-buffers-list)
  "Helm sources with buffer names to which icons should be added."
  :type '(repeat symbol))

(defun helm-all-the-icons--icon-for-buffer (buffer)
  "Get icon info for BUFFER."
  (if-let ((bfn (buffer-file-name buffer))
           (m (all-the-icons-auto-mode-match?)))
      (all-the-icons-icon-for-file (file-name-nondirectory bfn))
    (let ((icon (all-the-icons-icon-for-mode (buffer-local-value 'major-mode buffer))))
      (if (stringp icon)
          icon
        "  "))))

(defun helm-all-the-icons-buffers-add-icons (candidates _source)
  "Add icon to buffers source.
CANDIDATES is the list of candidates."
  (cl-loop for (display . buffer) in candidates
           collect (cons
                    (concat
                     (helm-all-the-icons--icon-for-buffer buffer)
                     (propertize " " 'display '(space :align-to 3))
                     display)
                    buffer)))

(defun helm-all-the-icons-files-add-icons (candidates _source)
  "Add icon to files source.
CANDIDATES is the list of candidates."
  (cl-loop for c in candidates
           collect
           (cl-destructuring-bind (display . filename) (if (listp c) c (cons c c))
             (cons (concat
                    (if (file-directory-p filename)
                        (all-the-icons-icon-for-dir filename)
                      (all-the-icons-icon-for-file filename))
                    (propertize " " 'display '(space :align-to 3))
                    display)
                   filename))))

(defun helm-all-the-icons-add-transformer (fn source)
  "Add FN to `filtered-candidate-transformer' slot of SOURCE."
  (setf (alist-get 'filtered-candidate-transformer source)
        (-uniq (append
                (-let [value (alist-get 'filtered-candidate-transformer source)]
                  (if (seqp value) value (list value)))
                (list fn)))))

;;;###autoload
(defun helm-all-the-icons-enable ()
  "Enable `helm-all-the-icons'."
  (interactive)
  (dolist (s helm-all-the-icons-files-sources)
    (helm-all-the-icons-add-transformer #'helm-all-the-icons-files-add-icons (symbol-value s)))
  (dolist (s helm-all-the-icons-buffer-sources)
    (helm-all-the-icons-add-transformer #'helm-all-the-icons-buffers-add-icons (symbol-value s))))

(provide 'helm-all-the-icons)
;;; helm-all-the-icons.el ends here
